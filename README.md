# Hugo meta module
This module provides data, partials, and shortcodes to display contact
information and other website metadata in a consistent, search engine optimized
way. You may use

* a data file that could be used as a template for your meta data and should be
  overridden in your website
* a frontmatter snippet that you can put in the `<head>` section of your site
* a partial to enforce that your configured meta data is complete
* snippets for various metadata to display contact information that is tagged
  with meta information according to [Schema.org](https://schema.org/)
* a `robots.txt` that automatically sets everything to `disallow` for
  non-production builds